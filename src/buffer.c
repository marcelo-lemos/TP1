////////////////////////////////////////////////////////////////////////////////
///
/// File:       buffer.c
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Source file for "buffer.h". This library contains the implementation
///     of the buffer that is used in the External Quicksort.
///
////////////////////////////////////////////////////////////////////////////////

/***************************** INCLUDED LIBRARIES *****************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book.h"
#include "buffer.h"

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Initializes a new bufferT.
///
/// Parameters:
///     size:   size of the buffer
///
/// Return:
///     A pointer to the new allocated buffer.
///
////////////////////////////////////////////////////////////////////////////////

bufferT * initializeBuffer(int size) {
    bufferT * newBuffer;
    int i;

    newBuffer = calloc(1, sizeof(bufferT));
    newBuffer->items = calloc(size, sizeof(bookT *));
    for(i = 0; i < size; i++)
        newBuffer->items[i] = initializeBook();
    newBuffer->size = size;
    newBuffer->count = 0;
    return newBuffer;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Frees a bufferT
///
/// Parameters:
///     buffer: pointer to the buffer that will be fred
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void freeBuffer(bufferT * buffer) {
    int i;

    for(i = 0; i < buffer->size; i++)
        freeBook(buffer->items[i]);
    free(buffer->items);
    free(buffer);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Adds a book to the buffer. The insertion keeps all items sorted.
///
/// Parameters:
///     buffer: buffer to add the book
///     book:   book that will be inserted in the buffer
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void addToBuffer(bufferT * buffer, bookT * book) {
    int index, i;

    // Get the position to insert the book
    for(i = 0; i < buffer->count; i++) {
        if(strcmp(book->title, buffer->items[i]->title) < 0) {
            break;
        }
    }
    index = i;
    // Move the other books
    if(buffer->count > 0) {
        for(i = buffer->count; i > index; i--)
            *(buffer->items[i]) = *(buffer->items[i - 1]);
    }
    // Insert the book
    *(buffer->items[index]) = *(book);
    buffer->count++;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Stores the book of minimum value and removes it from the buffer.
///
/// Parameters:
///     buffer: buffer to remove the book
///     book:   where to store the item that will be removed
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void removeMin(bufferT * buffer, bookT * book) {
    int i;

    *(book) = *(buffer->items[0]);
    buffer->count--;
    for(i = 0; i < buffer->count; i++) {
        *(buffer->items[i]) = *(buffer->items[i + 1]);
    }
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Stores the book of maximum value and removes it from the buffer.
///
/// Parameters:
///     buffer: buffer to remove the book
///     book:   where to store the item that will be removed
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void removeMax(bufferT * buffer, bookT * book) {
    *(book) = *(buffer->items[buffer->count - 1]);
    buffer->count--;
}
