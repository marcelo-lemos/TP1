////////////////////////////////////////////////////////////////////////////////
///
/// File:       book.c
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Source file for "book.h". This library contains the structures for the
///     books and functions directly related to them.
///
////////////////////////////////////////////////////////////////////////////////

/***************************** INCLUDED LIBRARIES *****************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "book.h"

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Initializes a new bookT.
///
/// Parameters:
///     --
///
/// Return:
///     A pointer do the new allocated book.
///
////////////////////////////////////////////////////////////////////////////////

bookT * initializeBook() {
    bookT * newBook;
    int i;

    newBook = (bookT *) malloc(sizeof(bookT));
    for(i = 0; i < (MAX_BOOK_TITLE + 1); i++) {
        newBook->title[i] = 0;
    }
    newBook->available = 0;
    return newBook;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Frees a book.
///
/// Parameters:
///     book:   book that will be fred
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void freeBook(bookT * book) {
    free(book);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Updates the title and the availability of a book.
///
/// Parameters:
///     title:      new title of the book
///     available:  new availability
///     book:       book to be updated
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void updateBook(char * title, char available, bookT * book) {
    strcpy(book->title, title);
    book->available = available;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Writes a book in a file. The book is first serialized.
///
/// Parameters:
///     book:   book to be serialized
///     file:   file where the book will be written
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void bookSerialize(bookT * book, FILE * file) {
    char metadata[MAX_BOOK_TITLE + 2];
    int i;

    for(i = 0; i < (MAX_BOOK_TITLE + 1); i++) {
        metadata[i] = book->title[i];
    }
    metadata[MAX_BOOK_TITLE + 1] = book->available;
    fwrite(&(metadata[0]), sizeof(char), (MAX_BOOK_TITLE + 2), file);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Reads a books from a file.
///
/// Parameters:
///     book:   where to store the book read
///     file:   where to read the book from
///
/// Return:
///     0 if the reading is successful, EOF otherwise.
///
////////////////////////////////////////////////////////////////////////////////

int bookDeserialize(bookT * book, FILE * file) {
    char metadata[MAX_BOOK_TITLE + 2];
    int i;

    if(fread(&(metadata[0]), sizeof(char), (MAX_BOOK_TITLE + 2), file) != (MAX_BOOK_TITLE + 2))
        return EOF;
    for(i = 0; i < (MAX_BOOK_TITLE + 1); i++) {
        book->title[i] = metadata[i];
    }
    book->available = metadata[MAX_BOOK_TITLE + 1];
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Prints a book in the desired output.
///
/// Parameters:
///     stream: where to print the book
///     book:   book to be printed
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void printBook(FILE * stream, bookT * book) {
    fprintf(stream, "%s %i\n", book->title, book->available);
}
