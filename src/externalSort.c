////////////////////////////////////////////////////////////////////////////////
///
/// File:       externalSort.c
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Source file for "externalSort.h". This library is responsible for
///     sorting the books using the External Quicksort algorithm.
///
////////////////////////////////////////////////////////////////////////////////

/***************************** INCLUDED LIBRARIES *****************************/

#include <stdio.h>
#include <string.h>

#include "book.h"
#include "buffer.h"
#include "externalSort.h"

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Reads the book in the desired position of a file.
///
/// Parameters:
///     file:   where to read the book from
///     pos:    position of the book in the file
///     book:   where the read book will be stored
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void readBook(FILE *file, int pos, bookT *book) {
    fseek(file, pos * sizeof(bookT), SEEK_SET);
    bookDeserialize(book, file);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Writes a book in the desired position of a file.
///
/// Parameters:
///     file:   where to write the book
///     pos:    position to write the book in the file
///     book:   book to be written/
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void writeBook(FILE *file, int pos, bookT *book) {
    fseek(file, pos * sizeof(bookT), SEEK_SET);
    bookSerialize(book, file);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function is responsible for the alternate reading of a file,
///     necessary for the External Quicksort. For that a flag is used to know
///     where the last book was read. It also checks if the reading positions
///     are the same as the writing position to avoid problems in the
///     writing step.
///
/// Parameters:
///     readTop:        position to read at the top of the file
///     writeTop:       position to write at the top of the file
///     readBottom:     position to read at the bottom of the file
///     writeBottom:    position to write at the bottom of the file
///     file:           file to read from
///     book:           book to store what was read
///     read:           flag that stores the last place that was read
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void readAlternating(int *readTop, int writeTop, int *readBottom, int writeBottom,FILE * file, bookT * book, int read) {
    if(*readTop == writeTop) {
        readBook(file, *readTop, book);
        *readTop = *readTop - 1;
        read = 0;
    } else if(*readBottom == writeBottom) {
        readBook(file, *readBottom, book);
        *readBottom = *readBottom + 1;
        read = 1;
    } else if(read) {
        readBook(file, *readTop, book);
        *readTop = *readTop - 1;
        read = 0;
    } else {
        readBook(file, *readBottom, book);
        *readBottom = *readBottom - 1;
        read = 1;
    }
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function is responsible for creating the partions of the file.
///     It reads the file alternating between top and bottom and writes the
///     smaller entities in the left partition and the bigger in the right one.
///     It also sorts the middle elements which fits in the internal memory.
///
/// Parameters:
///     file:   file that is being sorted
///     left:   left limit of the current sorting
///     right:  right limit of the current sorting
///     buffer: buffer for storing and sorting the books in internal memory
///     i:      right limit of the next left partiotion
///     j:      left limit of the next right partition
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void partition(FILE *file, int left, int right, bufferT * buffer, int *i, int *j) {
    int readTop, readBottom, writeTop, writeBottom;
    int read;
    bookT *book;

    readBottom = left;
    readTop = right;
    writeBottom = left;
    writeTop = right;
    read = 1;
    *i = left - 1;
    *j = right + 1;
    book = initializeBook();
    while(readBottom <= readTop) {
        // Reading
        readAlternating(&readTop, writeTop, &readBottom, writeBottom, file, book, read);
        // Insertion
        // If buffer has space left
        if(buffer->count < buffer->size - 1) {
            addToBuffer(buffer, book);
        // If book > buffer.max
        } else if(strcmp(book->title, buffer->items[buffer->size-2]->title) > 0) {
            *j = writeTop;
            writeBook(file, writeTop, book);
            writeTop--;
        // If book < buffer.min
        } else if(strcmp(book->title, buffer->items[0]->title) < 0) {
            *i = writeBottom;
            writeBook(file, writeBottom, book);
            writeBottom++;
        // Insert the book in the buffer and then write another one in the file
        } else {
            addToBuffer(buffer, book);
            // Keeps the partitions balanced
            if(writeBottom - left < right - writeTop) {
                removeMin(buffer, book);
                writeBook(file, writeBottom, book);
                writeBottom++;
            } else {
                removeMax(buffer, book);
                writeBook(file, writeTop, book);
                writeTop--;
            }
        }
    }
    // Write the buffer in the file
    while(writeBottom <= writeTop) {
        removeMax(buffer, book);
        writeBook(file, writeTop, book);
        writeTop--;
    }
    freeBook(book);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This is the main function for the External Quicksort. It is responsible
///     for checking if the current partition is valid for sorting, and for
///     the recursion of the algorithm.
///
/// Parameters:
///     file:   file to be sorted
///     left:   left limit of the sorting
///     right:  right limit of the sorting
///     buffer: buffer used on the partitioning step
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void externalQuicksort(FILE *file, int left, int right, bufferT * buffer) {
    int i, j;

    if((right - left) < 1)
        return;
    // Partitioning step
    partition(file, left, right, buffer, &i, &j);
    // Sort the smaller partition first
    if(i - left < right - j){
        externalQuicksort(file, left, i, buffer);
        externalQuicksort(file, j, right, buffer);
    } else {
        externalQuicksort(file, j, right, buffer);
        externalQuicksort(file, left, i, buffer);
    }
}
