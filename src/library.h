////////////////////////////////////////////////////////////////////////////////
///
/// File:       library.h
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Header file for "library.h". This file implements all functions
///     directly related to the library tasks, including getting the books,
///     sorting the books, managing the searches, etc.
///
////////////////////////////////////////////////////////////////////////////////

#ifndef _LIBRARY_H_
#define _LIBRARY_H_

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function is responsible for getting the variables related to the
///     sizes of the library.
///
/// Parameters:
///     librarySize:    number of books of the library
///     memorySize:     internal memory capacity
///     numberShelves:  number of sheves of the library
///     shelfSize:      shelf capacity
///     numberQueries:  number of queries to be done
///
/// Return:
///     0 if the reading is successful, -1 otherwise.
///
////////////////////////////////////////////////////////////////////////////////

int getSizes(int *librarySize, int *memorySize, int *numberShelves, int *shelfSize, int *numberQueries);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function is responsible for getting the books from the main input
///     and storing them on the desired file.
///
/// Parameters:
///     librarySize:    number of books
///     books:          file where the books will be stored
///
/// Return:
///     0 if the reading is successful, -1 otherwise.
///
////////////////////////////////////////////////////////////////////////////////

int getBooks(int librarySize, FILE * books);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function manages the sorting of the library. It uses the function
///     "externalQuicksort" from "externalSort.h" for that.
///
/// Parameters:
///     file:           file where the books are stored
///     librarySize:    number of books on the library
///     memorySize:     internal memory capacity
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void sortBooks(FILE * file, int librarySize, int memorySize);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function creates the shelves for the library and stores the books
///     on them.
///
/// Parameters:
///     file:           file where the books are currently stored
///     numberShelves:  number of shelves of the library
///     shelfSize:      shelf capacity
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void makeShelves(FILE * file, int numberShelves, int shelfSize);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function makes the index of the library, writing the first and the
///     last book of each shelf into a file.
///
/// Parameters:
///     numberShelves:  number of shelves of the library
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void makeIndex(int numberShelves);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function manages the queries of the library. For that it uses the
///     function "searchBook" from "binarySearch.h" to find the books.
///
/// Parameters:
///     numberQueries:  number of queries to be done
///
/// Return:
///     0 if the queries are successful, -1 otherwise.
///
////////////////////////////////////////////////////////////////////////////////

int doQueries(int numberQueries);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function prints all books stored in a file in the desired output.
///
/// Parameters:
///     stream: where to print the books
///     books:  file where the books are stored
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void printAllBooks(FILE * stream, FILE * books);

#endif  /* _LIBRARY_H_ */
