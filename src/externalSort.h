////////////////////////////////////////////////////////////////////////////////
///
/// File:       externalSort.h
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Header file for "externalSort.h". This library is responsible for
///     sorting the books using the External Quicksort algorithm.
///
////////////////////////////////////////////////////////////////////////////////

#ifndef _EXTERNAL_SORT_H_
#define _EXTERNAL_SORT_H_

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This is the main function for the External Quicksort. It is responsible
///     for checking if the current partition is valid for sorting, and for
///     the recursion of the algorithm.
///
/// Parameters:
///     file:   file to be sorted
///     left:   left limit of the sorting
///     right:  right limit of the sorting
///     buffer: buffer used on the partitioning step
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void externalQuicksort(FILE *file, int left, int right, bufferT * buffer);

#endif  /* _EXTERNAL_SORT_H_ */
