////////////////////////////////////////////////////////////////////////////////
///
/// File:       library.c
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Source file for "library.h". This file implements all functions
///     directly related to the library tasks, including getting the books,
///     sorting the books, managing the searches, etc.
///
////////////////////////////////////////////////////////////////////////////////

/***************************** INCLUDED LIBRARIES *****************************/

#include <stdio.h>

#include "book.h"
#include "buffer.h"
#include "externalSort.h"
#include "binarySearch.h"
#include "library.h"

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function is responsible for getting the variables related to the
///     sizes of the library.
///
/// Parameters:
///     librarySize:    number of books of the library
///     memorySize:     internal memory capacity
///     numberShelves:  number of sheves of the library
///     shelfSize:      shelf capacity
///     numberQueries:  number of queries to be done
///
/// Return:
///     0 if the reading is successful, -1 otherwise.
///
////////////////////////////////////////////////////////////////////////////////

int getSizes(int *librarySize, int *memorySize, int *numberShelves, int *shelfSize, int *numberQueries) {
    if(scanf("%d %d %d %d %d\n", librarySize, memorySize, numberShelves, shelfSize, numberQueries) == EOF)
        return -1;
    else
        return 0;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function is responsible for getting the books from the main input
///     and storing them on the desired file.
///
/// Parameters:
///     librarySize:    number of books
///     books:          file where the books will be stored
///
/// Return:
///     0 if the reading is successful, -1 otherwise.
///
////////////////////////////////////////////////////////////////////////////////

int getBooks(int librarySize, FILE * books) {
    int i;
    bookT * buffer;
    char title[MAX_BOOK_TITLE + 1];
    unsigned char available;

    fseek(books, 0, SEEK_SET);
    buffer = initializeBook();
    for(i = 0; i < librarySize; i++) {
        if(scanf("%s %hhu\n", title, &available) == EOF) {
            freeBook(buffer);
            return -1;
        }
        updateBook(title, available, buffer);
        bookSerialize(buffer, books);
    }
    freeBook(buffer);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function manages the sorting of the library. It uses the function
///     "externalQuicksort" from "externalSort.h" for that.
///
/// Parameters:
///     file:           file where the books are stored
///     librarySize:    number of books on the library
///     memorySize:     internal memory capacity
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void sortBooks(FILE * file, int librarySize, int memorySize) {
    bufferT *buffer;

    buffer = initializeBuffer(memorySize - 1);
    externalQuicksort(file, 0, (librarySize - 1), buffer);
    freeBuffer(buffer);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function creates the shelves for the library and stores the books
///     on them.
///
/// Parameters:
///     file:           file where the books are currently stored
///     numberShelves:  number of shelves of the library
///     shelfSize:      shelf capacity
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void makeShelves(FILE * file, int numberShelves, int shelfSize) {
    int i, j;
    char shelfName[20];
    bookT * buffer;
    FILE * shelf;

    buffer = initializeBook();
    for(i = 0; i < numberShelves; i++) {
        sprintf(shelfName, "estante%d", i);
        shelf = fopen(shelfName, "wb");
        for(j = 0; j < shelfSize; j++) {
            fseek(file, (((i * shelfSize) + j) * (MAX_BOOK_TITLE + 2)), SEEK_SET);
            if(!bookDeserialize(buffer, file)) {
                fseek(shelf, 0, SEEK_END);
                bookSerialize(buffer, shelf);
            }
        }
        fclose(shelf);
    }
    freeBook(buffer);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function makes the index of the library, writing the first and the
///     last book of each shelf into a file.
///
/// Parameters:
///     numberShelves:  number of shelves of the library
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void makeIndex(int numberShelves) {
    int i;
    char shelfName[20];
    bookT * buffer;
    FILE * index;
    FILE * shelf;

    index = fopen("indice", "wb");
    buffer = initializeBook();
    for(i = 0; i < numberShelves; i++) {
        sprintf(shelfName, "estante%d", i);
        shelf = fopen(shelfName, "r");
        fseek(shelf, 0, SEEK_END);
        if(ftell(shelf) == 0)
            fprintf(index, "#\n");
        else {
            fseek(shelf, 0, SEEK_SET);
            bookDeserialize(buffer, shelf);
            fprintf(index, "%s ", buffer->title);
            fseek(shelf, -(sizeof(bookT)), SEEK_END);
            bookDeserialize(buffer, shelf);
            fprintf(index, "%s\n", buffer->title);
        }
        fclose(shelf);
    }
    freeBook(buffer);
    fclose(index);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function manages the queries of the library. For that it uses the
///     function "searchBook" from "binarySearch.h" to find the books.
///
/// Parameters:
///     numberQueries:  number of queries to be done
///
/// Return:
///     0 if the queries are successful, -1 otherwise.
///
////////////////////////////////////////////////////////////////////////////////

int doQueries(int numberQueries) {
    char buffer[MAX_BOOK_TITLE + 1];
    int i;

    for(i = 0; i < numberQueries; i++) {
        if(scanf("%s\n", buffer) == EOF)
            return -1;
        searchBook(buffer);
    }
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function prints all books stored in a file in the desired output.
///
/// Parameters:
///     stream: where to print the books
///     books:  file where the books are stored
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void printAllBooks(FILE * stream, FILE * books) {
    bookT * buffer;

    buffer = initializeBook();
    fseek(books, 0, SEEK_SET);
    while(!bookDeserialize(buffer, books))
        printBook(stream, buffer);
    freeBook(buffer);
}
