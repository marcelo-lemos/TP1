////////////////////////////////////////////////////////////////////////////////
///
/// File:       buffer.h
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Source file for "buffer.h". This library contains the implementation
///     of the buffer that is used in the External Quicksort.
///
////////////////////////////////////////////////////////////////////////////////

#ifndef _BUFFER_H_
#define _BUFFER_H_

/***************************** TYPES & STRUCTURES *****************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Structure of the buffer used in External Quicksort.
///
/// Contents:
///     items:  array of books
///     size:   maximum size of the buffer
///     count:  number of current elements in the buffer
///
////////////////////////////////////////////////////////////////////////////////

typedef struct buffer {
    bookT **items;
    int size;
    int count;
} bufferT;

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Initializes a new bufferT.
///
/// Parameters:
///     size:   size of the buffer
///
/// Return:
///     A pointer to the new allocated buffer.
///
////////////////////////////////////////////////////////////////////////////////

bufferT * initializeBuffer(int size);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Frees a bufferT
///
/// Parameters:
///     buffer: pointer to the buffer that will be fred
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void freeBuffer(bufferT * buffer);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Adds a book to the buffer. The insertion keeps all items sorted.
///
/// Parameters:
///     buffer: buffer to add the book
///     book:   book that will be inserted in the buffer
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void addToBuffer(bufferT * buffer, bookT * book);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Stores the book of minimum value and removes it from the buffer.
///
/// Parameters:
///     buffer: buffer to remove the book
///     book:   where to store the item that will be removed
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void removeMin(bufferT * buffer, bookT * book);

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Stores the book of maximum value and removes it from the buffer.
///
/// Parameters:
///     buffer: buffer to remove the book
///     book:   where to store the item that will be removed
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void removeMax(bufferT * buffer, bookT * book);

#endif  /* _BUFFER_H_ */
