////////////////////////////////////////////////////////////////////////////////
///
/// File:       main.c
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     File containing the main function for tp1. The goal of tp1 is to
///     implement a program that organizes and manages a library.
///
////////////////////////////////////////////////////////////////////////////////

/***************************** INCLUDED LIBRARIES *****************************/

#include <stdio.h>

#include "library.h"

/************************************ MAIN ************************************/

int main(void) {
    int librarySize;
    int memorySize;
    int numberShelves;
    int shelfSize;
    int numberQueries;
    FILE * books;
    FILE * organized;

    // File for getting all the books and sorting them before they are put into shelves
    books = tmpfile();
    organized = fopen("livros_ordenados", "w");
    getSizes(&librarySize, &memorySize, &numberShelves, &shelfSize, &numberQueries);
    getBooks(librarySize, books);
    sortBooks(books, librarySize, memorySize);
    makeShelves(books, numberShelves, shelfSize);
    makeIndex(numberShelves);
    doQueries(numberQueries);
    printAllBooks(organized, books);
    fclose(books);
    fclose(organized);

    return 0;
}
