# Executable name
EXEC = tp1

# Compiler and flags
CC = gcc
CFLAGS = -O2 -Wall -Wextra -Werror -std=c99 -pedantic

# Source and object directories
SRCDIR = src
OBJDIR = obj

# Sources, includes, and objects
SRC := $(wildcard $(SRCDIR)/*.c)
DEP := $(wildcard $(SRCDIR)/*.h)
OBJ := $(patsubst %.c,$(OBJDIR)/%.o,$(notdir $(SRC)))

# Valgrind flags
VFLAGS = --leak-check=full --leak-resolution=high --show-reachable=yes --track-origins=yes

# Input for tests
INPUT = entradas/entrada15

# Output for tests
OUTPUT = output

# Output directory for tests
OUTDIR = out

# Input generator
GEN = generator

# Test execution command
TEST = ./$(EXEC) < $(INPUT) > $(OUTDIR)/$(OUTPUT)


all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) $(CFLAGS) $^ -o $@

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(DEP)
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $< -o $@

run: $(EXEC)
	@mkdir -p $(OUTDIR)
	$(TEST)
	@mv estante* $(OUTDIR)/
	@mv indice $(OUTDIR)/
	@mv livros_ordenados $(OUTDIR)/

valgrind: $(EXEC)
	@mkdir -p $(OUTDIR)
	valgrind $(VFLAGS) $(TEST)
	@mv estante* $(OUTDIR)/
	@mv indice $(OUTDIR)/
	@mv livros_ordenados $(OUTDIR)/

doc.pdf: FORCE
	@cd doc && $(MAKE) && mv doc.pdf ..

$(GEN): gen/generator.c
	$(CC) $(CFLAGS) $^ -o $@

tp1.zip: doc.pdf
	mkdir -p zip/$(EXEC)
	cp -ar makefile src doc.pdf zip/$(EXEC)
	@cd zip && zip -r $(EXEC).zip $(EXEC) && mv $@ ..
	rm -rf zip

clean: cleanobj cleanout cleandoc cleangen cleanzip
	rm -f $(EXEC)

cleanobj:
	rm -rf $(OBJDIR)

cleanout:
	rm -rf $(OUTDIR)

cleandoc:
	rm -f doc.pdf

cleangen:
	rm -f $(GEN)

cleanzip:
	rm -f $(EXEC).zip

.PHONY: run valgrind clean cleanobj cleanout cleandoc cleangen cleanzip

FORCE:
