\section{Modelagem do Problema}
O problema da biblioteca pode ser dividido em duas partes principais:
\begin{compactenum}
    \item \textbf{Organizar o acervo}: Ordenar os livros em ordem lexicográfica e separá-los
    em estantes.
    \item \textbf{Pesquisar no acervo}: Realizar a busca de um livro para atender a um novo pedido.
\end{compactenum}
\subsection{Organizar o Acervo}
Para organizar o acervo, precisamos utilizar um algoritmo de ordenação externa, uma
vez que ele é grande demais para ser armazenado em memória primária. O algoritmo escolhido
para realizar este trabalho foi o \emph{Quicksort Externo}.

O Quicksort Externo é um algoritmo de ordenação \emph{in situ} (executa as
permutações das entidades no próprio arquivo, sem necessitar de arquivos auxiliares), que
parte do princípio de dividir e conquistar para realizar sua ordenação.

Queremos ordenar um arquivo
$$ A=R_1,R_2,R_3,\cdots,R_{N-2},R_{N-1},R_N $$
em que $R_i$ é o registro que se encontra na i-ésima posição de $A$.

O primeiro passo do algoritmo consiste em particionar $A$ tal que
\begin{center}
    \begin{tabular}{cccc}
        \cline{2-2}
        \cline{4-4}
        \rule{0pt}{2.5ex}
        $A =$ & \multicolumn{1}{|c|}{$R_1, \cdots, R_i$} & {$\le R_{i+1} \le R_{i+2} \le \cdots \le R_{i+M-1} \le R_{i+M} \le$} & \multicolumn{1}{|c|}{$R_{i+M+1}, \cdots, R_N$} \\
        \cline{2-2}
        \cline{4-4}
        \rule{0pt}{4ex}
         & $A'_1$ &  & $A'_2$
    \end{tabular}
\end{center}
de modo que os subarquivos $A'_1$ e $A'_2$ contenham os registros menores que $R_{i+1}$ e maiores que
$R_{i+M}$ respectivamente. Em seguida o algoritmo é chamado recursivamente para cada um dos subarquivos
gerados, sendo que o de menor tamanho é ordenado primeiro. Esta condição é necessária para que, em média, o número de subarquivos
com processamento adiado não ultrapasse $\log n$. Os registros ordenados $R_{i+1},...,R_{i+M}$
correspondem ao pivô do algoritmo, encontrando-se em memória primária durante sua execução.
Portanto arquivos de tamanho até $M$ são ordenados em um único passo. Arquivos com um único
registro ou vazios são ignorados, uma vez que não há o que ordenar.

Um pseudocódigo do procedimento é mostrado abaixo:

\begin{algorithm}[H]
    \small
    \input{algorithms/quicksortExterno.tex}
\end{algorithm}

O objetivo do procedimento \textsc{Particao} no Algoritmo 1 é permutar os registros menores que
$R_{i+1}$ para $A'_1$, e os registros maiores que $R_{i+M}$ para $A'_2$. Assim podemos determinar os pontos
de partição $i$ e $j$.

No processo de partição a leitura é controlada por dois apontadores: $l_I$ e $l_S$
(inferior e superior). Estes apontadores são inicializados nos extremos da partição atual,
no início e no final respectivamente. Toda vez que um registro é lido no início do arquivo, $l_I$ avança uma posição.
Toda vez que um registro é lido no final, $l_S$ retrocede uma posição. A escrita é controlada de forma semelhante pelos apontadores
$e_I$ e $e_S$. Sempre que um registro é escrito no início, $e_I$ avança uma posição. Sempre que um registro é escrito no
final, $e_S$ retrocede uma posição.

Um registro é lido ora no início do arquivo ora no final. Esse processo
se repete até que o pivô (chamado de \emph{buffer} daqui em diante) possua $M-1$ elementos. A partir desse momento, os
próximos registros a serem lidos serão sempre comparados com o máximo e mínimo do \emph{buffer}. Caso o último
registro lido seja maior que o máximo, ele será escrito em $A'_2$, na posição indicada por $e_S$. Um processo
parecido ocorre caso ele seja menor que o mínimo. Ele é escrito em $A'_1$ na posição indicada por $e_I$.
Caso nenhuma das duas condições anteriores sejam satisfeitas, este registro é inserido no \emph{bufffer} e em seguida o
máximo ou mínimo presente em memória primária é escrito no arquivo. Para decidir qual dos dois será escrito, optamos por
deixar as partições balanceadas, e para isso, verificamos neste momento qual das duas partições possui mais registros até
o momento, e então escrevemos na outra. O mínimo caso $A'_1$ esteja menor, ou o máximo caso $A'_2$ esteja menor. A leitura
continua até que todos os registros do arquivo sejam lidos. Após isso, os registros que ainda estão no buffer são escritos
nas posições restantes.

Um pseudocódigo do algoritmo é mostrado a seguir:

\begin{algorithm}[H]
    \small
    \input{algorithms/particao.tex}
\end{algorithm}

\subsection{Pesquisar no Acervo}
A pesquisa consiste duas partes:
\begin{compactenum}
    \item Determinar em qual estante o livro está localizado
    \item Determinar em qual posição o livro se encontra na estante
\end{compactenum}

A primeira parte é uma simples busca linear, onde percorremos o índice, da primeira à
última estantes, comparando o título do livro desejado com o título do primeiro e do último
livro de cada estante, na tentativa de determinar em qual ele estaria localizado.

Já na segunda etapa, devemos percorrer a estante encontrada no passo anterior para definir
em qual posição o livro está localizado. Um algoritmo de Pesquisa Binária é utilizado
para tornar a busca mais eficiente.

A Pesquisa Binária, assim como o Quicksort Externo, também utiliza o método de dividir e
conquistar. Temos três apontadores para controlar a pesquisa binária:
\begin{compactitem}
    \item \textbf{esquerda}: define o limite inferior da busca.
    \item \textbf{direita}: define o limite superior da busca.
    \item \textbf{meio}: aponta para o item central da partição atual.
\end{compactitem}

Primeiramente temos que definir o valor de \emph{meio}. A cada iteração ele receberá a média
dos valores de \emph{esquerda} e \emph{direita}. Em seguida comparamos o livro que queremos encontrar
com o item central da estante. Caso seja maior sabemos que o livro que estamos procurando está na parte
superior ao item central, e portanto, atualizamos o valor de \emph{esquerda} para $meio+1$. Realizamos
um processo semelhante caso nosso livro seja menor que o central: atualizamos o valor de \emph{direita}
para $meio-1$. Em seguida encontramos o novo valor de \emph{meio} e realizamos os mesmos procedimentos, até
que o livro desejado seja encontrado ou determine-se que ele não está presente na estante. O Algoritmo 3
é um pseudocódigo para a Pesquisa Binária.

\begin{algorithm}[H]
    \small
    \input{algorithms/pesquisaBinaria.tex}
\end{algorithm}
