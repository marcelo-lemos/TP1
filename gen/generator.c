#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_BOOK_TITLE 20

int main(int argc, char const *argv[]) {
    int key;
    int nameSize;
    int loopSize;
    FILE * output;
    int i, j;
    const char alphabet[] = "_abcdefghijklmnopqrstuvwxyz";  // alphabet used

    if(argc != 7) {
        printf("Numero de argumentos incompativel!\n");
        exit(1);
    }
    output = fopen(argv[6], "w");
    srand(time(NULL));
    fprintf(output, "%s %s %s %s %s\n", argv[1], argv[2], argv[3], argv[4], argv[5]);
    loopSize = atoi(argv[1]);       // number of books
    for(i = 0; i < loopSize; i++) {
        nameSize = (rand() % MAX_BOOK_TITLE) + 1;       // generate size of book name
        // generate random book name
        for(j = 0; j < nameSize; j++) {
            key = (rand() % 27);
            fprintf(output, "%c", alphabet[key]);
        }
        key = rand() % 2;       // generate value of book.available
        fprintf(output, " %d\n", key);
    }
    loopSize = atoi(argv[5]);       // number of queries
    for(i = 0; i < loopSize; i++) {
        nameSize = (rand() % MAX_BOOK_TITLE) + 1;       // generate size of book name
        // generate random book name
        for(j = 0; j < nameSize; j++) {
            key = (rand() % 27);
            fprintf(output, "%c", alphabet[key]);
        }
        fprintf(output, "\n");
    }
    return 0;
}
